import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Component } from 'react';

const ColorBlock = styled.div`
  width: 50px;
  height: 50px;
  color: #eeeeee;
  border: 2px solid #ccc;
  border-radius: 5px;
`;

const ButtonBase = styled.a`
  text-decoration: none;
  display: inline-block;
`;

const SimpleButton = styled(ButtonBase)`
  color: #ccc;
  height: 18px;
  border-radius: 5px;
  padding: 5px 10px;
`

const Wrapper = styled.div`
  display: block;
`

class ColorOptions extends Component<{setColor: any}, {selectedColor: string}> {
  state = {
    selectedColor: null
  }

  render() {
    return (
      <Wrapper>
        <SimpleButton
          style={{ background: "#ffff00", cursor: this.state.selectedColor !== "#ffff00" ? 'pointer' : 'default'  }}
          href="#"
          onClick={() => {
            console.info("Kolor zmieniony!");
            this.props.setColor("#ffff00");
            this.setState({selectedColor: "#ffff00"})
          }}
        >
          yellow
        </SimpleButton>
        &nbsp;&nbsp;
        <SimpleButton
          style={{ background: "#00ff00", cursor: this.state.selectedColor !== "#00ff00" ? 'pointer' : 'default'  }}
          href="#"
          onClick={() => {
            console.info("Kolor zmieniony!");
            this.props.setColor("#00ff00");
            this.setState({selectedColor: "#00ff00"})
          }}
        >
          green
        </SimpleButton>
        &nbsp;&nbsp;
        <SimpleButton
          style={{ background: "#0000ff", cursor: this.state.selectedColor !== "#0000ff" ? 'pointer' : 'default' }}
          href="#"
          onClick={() => {
            console.info("Kolor zmieniony!");
            this.props.setColor("#0000ff");
            this.setState({selectedColor: "#0000ff"})
          }}
        >
          blue
        </SimpleButton>
        &nbsp;&nbsp;
        <SimpleButton
          style={{ background: "#ff0000", cursor: this.state.selectedColor !== "#ff0000" ? 'pointer' : 'default'  }}
          href="#"
          onClick={() => {
            console.info("Kolor zmieniony!");
            this.props.setColor("#ff0000");
            this.setState({selectedColor: "#ff0000"})
          }}
        >
          red
        </SimpleButton>
      </Wrapper>
    )
  }
}

export const ColorPicker = () => {
  const [color, setColor] = useState("");
  const [showInTitle, setShowInTitle] = useState(true);

  useEffect(() => {
    if (color !== "") {
      setShowInTitle(true);
    } else if (color === "") {
      setShowInTitle(false);
    }
    if (showInTitle) document.title = "Wybrany kolor: " + color;
  });


  return (
    <div>
      Wybrany kolor: <ColorBlock style={{ background: color }} />
      <br />
      kolory:
      <br />
      <ColorOptions setColor={setColor} />
    </div>
  );
};

export default ColorPicker;